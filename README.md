# easy-rsa registry

## Read certs from registry file

```rust
let registry_file_path = Path::new("index.txt");
let registry_file_path = format!("{}", registry_file_path.display());
let certs = read_certs_from_file(&registry_file_path)?;
```

## Update cert status

```rust
let registry_file_path = Path::new("index.txt");
let registry_file_path = format!("{}", registry_file_path.display());
let cert_serial: &str = "36593F7437AEB5007D2953E7A98B7601";
let certs = update_cert_status(&registry_file_path, &cert_serial, EasyRsaCertificateStatus::Revoked)?;
```