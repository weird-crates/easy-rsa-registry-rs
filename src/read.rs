use std::fs::File;
use std::io::{BufRead, BufReader};
use std::str::FromStr;

use log::{debug, error, info};

use crate::{EasyRsaCertificate, EasyRsaCertificateStatus, EasyRsaCertificateStatusCode};
use crate::error::EasyRsaRegistryError;

pub fn read_certs_from_file(index_file_path: &str) -> Result<Vec<EasyRsaCertificate>, EasyRsaRegistryError> {
    info!("read certificates from registry file '{index_file_path}'");

    let f = File::open(index_file_path)?;
    let mut reader = BufReader::new(f);

    let mut buf = String::new();

    let mut certs: Vec<EasyRsaCertificate> = vec![];

    let mut parse_error = false;

    let mut result = reader.read_line(&mut buf)?;

    while result > 0 {
        buf = buf.replace("\t\t", "\t");

        let parts = buf.split("\t").collect::<Vec<&str>>();

        let certificate_subject = parts.last().unwrap().to_string().replace("\n", "");

        if parts.len() == 5 {
            let cert = EasyRsaCertificate {
                status: EasyRsaCertificateStatus::from_str(parts.get(0).unwrap())?,
                expiration_date: parts.get(1).unwrap().to_string(),
                revocation_date: None,
                serial_number: parts.get(2).unwrap().to_string(),
                status_code: EasyRsaCertificateStatusCode::from_str(parts.get(3).unwrap())?,
                certificate_subject,
            };

            certs.push(cert);

        } else if parts.len() == 6 {
            let cert = EasyRsaCertificate {
                status: EasyRsaCertificateStatus::from_str(parts.get(0).unwrap())?,
                expiration_date: parts.get(1).unwrap().to_string(),
                revocation_date: Some(parts.get(2).unwrap().to_string()),
                serial_number: parts.get(3).unwrap().to_string(),
                status_code: EasyRsaCertificateStatusCode::from_str(parts.get(4).unwrap())?,
                certificate_subject,
            };

            certs.push(cert);

        } else {
            error!("unsupported registry format");
            parse_error = true;
            break;
        }

        buf = String::new();

        result = reader.read_line(&mut buf)?;
    }

    debug!("certs: {:?}", certs);

    if !parse_error {
        Ok(certs)

    } else {
        Err(EasyRsaRegistryError::ParseError)
    }
}

#[cfg(test)]
mod tests {
    use std::path::Path;

    use crate::{EasyRsaCertificateStatus, EasyRsaCertificateStatusCode};
    use crate::error::EasyRsaRegistryError;
    use crate::read::read_certs_from_file;

    #[test]
    fn certs_should_be_read() {
        let registry_file = Path::new("test-data").join("index.txt");
        let registry_file = format!("{}", registry_file.display());

        let certs = read_certs_from_file(&registry_file).unwrap();

        assert_eq!(13, certs.len());

        let change_me_cert = certs.first().unwrap();

        assert_eq!(EasyRsaCertificateStatus::Valid, change_me_cert.status);
        assert!(change_me_cert.revocation_date.is_none());
        assert_eq!("230820121339Z", change_me_cert.expiration_date);
        assert_eq!("F15621FE882946FC63FA657837888821", change_me_cert.serial_number);
        assert_eq!(EasyRsaCertificateStatusCode::Unknown, change_me_cert.status_code);
        assert_eq!("/CN=ChangeMe", change_me_cert.certificate_subject);
    }

    #[test]
    fn return_error_for_unsupported_file_format() {
        let registry_file = Path::new("test-data").join("invalid.txt");
        let registry_file = format!("{}", registry_file.display());

        match read_certs_from_file(&registry_file) {
            Err(e) => {
                match e {
                    EasyRsaRegistryError::ParseError => assert!(true),
                    _ => panic!("parse error expected"),
                }
            }
            Ok(_) => panic!("error expected")
        }
    }

    #[test]
    fn return_error_for_non_existing_file() {
        match read_certs_from_file("unknown-file") {
            Err(e) => {
                match e {
                    EasyRsaRegistryError::IOError(_) => assert!(true),
                    _ => panic!("io error expected"),
                }
            }
            Ok(_) => panic!("error expected")
        }
    }
}