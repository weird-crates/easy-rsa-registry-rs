use std::str::FromStr;

use crate::error::EasyRsaRegistryError;

pub mod read;
pub mod update;

pub mod error;

#[derive(PartialEq,Clone,Debug)]
pub struct EasyRsaCertificate {
    pub status: EasyRsaCertificateStatus,

    /// Format: `YYMMDDHHMMSSZ`
    pub expiration_date: String,

    pub revocation_date: Option<String>,

    pub serial_number: String,

    pub status_code: EasyRsaCertificateStatusCode,

    /// Example: `/CN=example-client`
    pub certificate_subject: String
}

impl EasyRsaCertificate {
    pub fn to_registry_row(&self) -> String {

        let mut revocation_date: String = String::new();

        if self.revocation_date.is_some() {
            let tmp = &self.revocation_date.clone().unwrap();
            revocation_date = tmp.to_string();
        }

        format!(
            "{}\t{}\t{}\t{}\t{}\t{}", self.status.to_string(), self.expiration_date.to_string(),
            revocation_date, self.serial_number.to_string(), self.status_code.to_string(),
            self.certificate_subject.to_string()
        )
    }
}

#[derive(PartialEq,Clone,Debug)]
pub enum EasyRsaCertificateStatus {
    /// Certificate is currently valid and has not been revoked.
    Valid,

    /// Certificate has been revoked and is no longer considered valid.
    Revoked,

    /// Certificate has reached its expiration date and is no longer considered valid.
    Expired,

    /// Indicates that the status of the certificate is unknown or hasn't been checked yet.
    Unknown
}

impl ToString for EasyRsaCertificateStatus {
    fn to_string(&self) -> String {
        match self {
            EasyRsaCertificateStatus::Valid => "V".to_string(),
            EasyRsaCertificateStatus::Expired => "E".to_string(),
            EasyRsaCertificateStatus::Revoked => "R".to_string(),
            EasyRsaCertificateStatus::Unknown => "X".to_string()
        }
    }
}

impl FromStr for EasyRsaCertificateStatus {
    type Err = EasyRsaRegistryError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let result = match s {
            "V" => EasyRsaCertificateStatus::Valid,
            "E" => EasyRsaCertificateStatus::Expired,
            "R" => EasyRsaCertificateStatus::Revoked,
            _ => EasyRsaCertificateStatus::Unknown,
        };

        Ok(result)
    }
}

#[derive(PartialEq,Clone,Debug)]
pub enum EasyRsaCertificateStatusCode {
    /// Certificate is currently valid and has not been revoked.
    Valid,

    /// Certificate has been revoked and is no longer considered valid.
    Revoked,

    /// Certificate has reached its expiration date and is no longer considered valid.
    Expired,

    /// Indicates that the status of the certificate is unknown or hasn't been checked yet.
    Unknown
}

impl ToString for EasyRsaCertificateStatusCode {
    fn to_string(&self) -> String {
        match &self {
            EasyRsaCertificateStatusCode::Valid => "01".to_string(),
            EasyRsaCertificateStatusCode::Expired => "02".to_string(),
            EasyRsaCertificateStatusCode::Revoked => "03".to_string(),
            _ => "unknown".to_string()
        }
    }
}

impl FromStr for EasyRsaCertificateStatusCode {
    type Err = EasyRsaRegistryError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let result = match s {
            "01" => EasyRsaCertificateStatusCode::Valid,
            "02" => EasyRsaCertificateStatusCode::Expired,
            "03" => EasyRsaCertificateStatusCode::Revoked,
            _ => EasyRsaCertificateStatusCode::Unknown,
        };

        Ok(result)
    }
}