use std::io;

use thiserror::Error;

#[derive(Error, Debug)]
pub enum EasyRsaRegistryError {
    #[error("io error")]
    IOError(#[from] io::Error),

    #[error("unsupported registry format")]
    ParseError
}