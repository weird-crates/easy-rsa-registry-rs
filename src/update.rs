use std::fs;

use chrono::Local;
use log::info;

use crate::EasyRsaCertificateStatus;
use crate::error::EasyRsaRegistryError;
use crate::read::read_certs_from_file;

pub fn update_cert_status(index_file_path: &str,
          cert_serial: &str, status: EasyRsaCertificateStatus) -> Result<(), EasyRsaRegistryError> {
    info!("update certificate status for '{cert_serial}' to {:?}", status);
    info!("index file path '{index_file_path}'");

    let certs = read_certs_from_file(&index_file_path)?;

    let mut rows: Vec<String> = vec![];

    for cert in certs {
        if cert.serial_number == cert_serial {
            let mut updated_cert = cert.clone();

            if status == EasyRsaCertificateStatus::Valid {
                updated_cert.revocation_date = None;
            }

            if status == EasyRsaCertificateStatus::Revoked {
                let now = Local::now();
                let now = now.format("%y%m%d%H%M%SZ");
                updated_cert.revocation_date = Some(format!("{now}"));
            }

            updated_cert.status = status.clone();
            rows.push(updated_cert.to_registry_row())

        } else {
            rows.push(cert.to_registry_row())
        }
    }

    let content = rows.join("\n");

    fs::write(index_file_path, content)?;

    info!("status has been updated for '{cert_serial}' to '{}'", status.to_string());

    Ok(())
}

#[cfg(test)]
mod tests {
    use std::fs;
    use std::path::Path;

    use crate::{EasyRsaCertificateStatus, EasyRsaCertificateStatusCode};
    use crate::error::EasyRsaRegistryError;
    use crate::read::read_certs_from_file;
    use crate::update::update_cert_status;

    #[test]
    fn return_parse_error_for_corrupted_file() {
        let registry_file = Path::new("test-data").join("invalid.txt");
        let registry_file = format!("{}", registry_file.display());

        match update_cert_status(
            &registry_file, "F15621FE882946FC63FA657837888821",
            EasyRsaCertificateStatus::Revoked) {
            Ok(_) => panic!("parse error expected"),
            Err(e) => {
                match e {
                    EasyRsaRegistryError::ParseError => assert!(true),
                    _ => panic!("parse error expected")
                }
            }
        }
    }

    #[test]
    fn status_should_be_updated_from_revoked_to_valid() {
        assert_status_changed("36593F7437AEB5007D2953E7A98B7601", EasyRsaCertificateStatus::Valid);
        assert_status_changed("F15621FE882946FC63FA657837888821", EasyRsaCertificateStatus::Expired);
        assert_status_changed("91453D05307390677E4848FDD0D812D4", EasyRsaCertificateStatus::Revoked);
    }

    fn assert_status_changed(cert_serial: &str, target_status: EasyRsaCertificateStatus) {
        let nominal_registry_file = Path::new("test-data").join("index.txt");
        let dest_registry_file = Path::new("test-data").join("index.txt_");

        if dest_registry_file.exists() {
            fs::remove_file(&dest_registry_file).unwrap();
        }

        fs::copy(nominal_registry_file, &dest_registry_file).unwrap();

        let dest_registry_file = format!("{}", dest_registry_file.display());

        update_cert_status(&dest_registry_file, cert_serial, target_status.clone()).unwrap();

        let certs = read_certs_from_file(&dest_registry_file).unwrap();

        let result = certs.iter().find(|c|
            c.serial_number == cert_serial).unwrap();

        assert_eq!(result.status, target_status.clone());
        assert_eq!(result.status_code, EasyRsaCertificateStatusCode::Unknown);
        assert_eq!(result.serial_number, cert_serial);

        if target_status == EasyRsaCertificateStatus::Revoked {
            assert!(result.revocation_date.is_some());

        } else {
            assert!(result.revocation_date.is_none());
        }
    }
}